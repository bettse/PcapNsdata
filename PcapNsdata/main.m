//
//  main.m
//  PcapNsdata
//
//  Created by Eric Betts on 3/7/15.
//  Copyright (c) 2015 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <pcap.h>
#import <errno.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import <netinet/if_ether.h>
#import <netinet/ip.h>
#import <netinet/ip6.h>
#import <netinet/tcp.h>
#import <netinet/udp.h>

//Globals, cause I don't give a fuck
pcap_t *descr;

void parsePayload(const unsigned char *payload, NSInteger len) {
    NSData *nspayload = [NSData dataWithBytes:payload length:len];
    //void *archive = (__bridge void *)([NSKeyedUnarchiver unarchiveObjectWithData:nspayload]);
    //NSLog(@"Dictionary: %@", archive);
    NSData *myData = [NSKeyedArchiver archivedDataWithRootObject:@{
        @"key1" : @"value1",
        @"key2" : @"value2"
    }];
    NSLog(@"archive: %@", myData);
}

void packet_callback(u_char *useless, const struct pcap_pkthdr *pkthdr, const u_char *packet) {
    //PacketProcessor *processor = [PacketProcessor sharedProcessor];
    if (packet == NULL) {
        NSLog(@"Didn't grab packet\n");
        return;
    }

    /* lets start with the ether header... */
    struct ether_header *eptr = (struct ether_header *)packet;

    /* check to see if we have an ip packet */
    if (ntohs(eptr->ether_type) == ETHERTYPE_IP) {
        //NSLog(@"IP Packet!");
        struct ip *iphdr = (struct ip *)(packet + ETHER_HDR_LEN);
        NSInteger ipHeaderSize = iphdr->ip_hl * sizeof(unsigned int);
        NSInteger ipLen = htons(iphdr->ip_len);
        if (iphdr->ip_p == IPPROTO_TCP) {
            //NSLog(@"\tTCP action!");
            struct tcphdr *tcphdr = (struct tcphdr *)(packet + ETHER_HDR_LEN + ipHeaderSize);
            NSInteger tcpHeaderSize = tcphdr->th_off * sizeof(unsigned int);
            NSInteger len = ipLen - (ipHeaderSize + tcpHeaderSize);
            if (len > 0) {
                parsePayload(packet + ETHER_HDR_LEN + ipHeaderSize + tcpHeaderSize, len);
            }
            //TCPPacket *tcpObject = [[TCPPacket alloc] initWithPacket:(u_char *)packet packetHeader:pkthdr];
            //[processor.allPackets addObject:tcpObject];
            //[processor.bufferPackets addObject:tcpObject];
        }
    } else if (ntohs(eptr->ether_type) == ETHERTYPE_IPV6) {
        struct ip6_hdr *iphdr = (struct ip6_hdr *)(packet + ETHER_HDR_LEN);
        if (iphdr->ip6_nxt == IPPROTO_UDP) {
            NSLog(@"UDP6 action!");
        } else if (iphdr->ip6_nxt == IPPROTO_TCP) {
            NSLog(@"TCP6 action!");
        }
    } else if (ntohs(eptr->ether_type) == ETHERTYPE_ARP) {
        NSLog(@"Ethernet type hex:%x dec:%d is an ARP packet\n", ntohs(eptr->ether_type), ntohs(eptr->ether_type));
    } else {
        NSLog(@"Ethernet type %x not IP\n", ntohs(eptr->ether_type));
    }
}

void openCapture(NSURL *fileURL) {
    //[self.allPackets removeAllObjects];
    char errbuf[PCAP_ERRBUF_SIZE];
    descr = pcap_open_offline([[fileURL path] UTF8String], errbuf);
    if (descr == NULL) {
        NSLog(@"[ERROR] %s", errbuf);
        return;
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul), ^{
        @autoreleasepool {
            int r = pcap_loop(descr, 0, packet_callback, NULL);
            if(r < 0) {
                return;
            }
        }
    });
}

int main(int argc, const char *argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
        NSURL *pcapfile = [NSURL URLWithString:@"alfred-remote.pcap"];
        openCapture(pcapfile);
        [[NSRunLoop currentRunLoop] run];
    }

    return 0;
}